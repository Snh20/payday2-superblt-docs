# SuperBLT

SuperBLT is a fork of the BLT Modloading hook for PAYDAY 2, with a number
of major improvements, such as a audio API and the ability to alter any
base-game XML file (completely removing the need for Bundle Modder).

## Installation
- Download and install the [Microsoft 2017 Visual C++ Redistributable](https://aka.ms/vs/15/release/VC_redist.x86.exe) (you
only need to do this once, even if you reinstall SuperBLT)
- Download the [Latest IPHLPAPI Release DLL](https://znix.xyz/random/payday-2/SuperBLT/latest-wsock.php), and place
it in your PAYDAY 2 folder.
- Run the game, and SuperBLT will ask you if you want to automatically download the basemod. Select yes, and it will
tell you when it's done downloading. At this point, SuperBLT is fully installed.

However, if you've previously used vanilla BLT, you will have to:
- Delete `IPHLPAPI.dll` - This is the original BLT DLL, and you can't have both installed at once
- Delete `mods/base` - This is another part of the original BLT, and SuperBLT will download it's own
version. However, you must delete the old version first.

## Notes about `IPHLPAPI.dll` vs `WSOCK32.dll`

Some computers, for reasons that are not apparent, will not load the `IPHLPAPI.dll` file as used by vanilla BLT. For
this reason, SuperBLT has switched over to hooking `WSOCK32.dll`, which I've yet to see any trouble from.

If for whatever reason you need an IPHLPAPI.dll version, you can download
the [IPHLPAPI Release DLL](https://znix.xyz/random/payday-2/SuperBLT/latest-release.php), and
install it like you would the normal (`WSOCK32.dll`) DLL.

You can also download the [Latest Development DLL](https://znix.xyz/random/payday-2/SuperBLT/latest.php),
however you should only do this if you've been instructed to or you know what you're doing. Note that if you
use this DLL, you'll always be told a newer version is available by the update notifier and as such you'll have
to manually check for updates.

## Credits
- ZNixian, for writing SuperBLT
- the Restoration Crew, for testing/feedback
- JamesWilko and SirWaddles, for writing the original BLT

## Source Code
This project is Free and Open Source software, with the DLL under the GNU General
Public License version 3 (or any later version), and the basemod under the BSD license.

[DLL Source Code](https://gitlab.com/znixian/payday2-superblt)

[Basemod Source Code](https://gitlab.com/znixian/payday2-superblt-lua)

## Developer Documentation

For everything not added by SuperBLT, see [the vanilla BLT's documentation](https://payday-2-blt-docs.readthedocs.io/en/latest/).

Documentation for stuff added by SuperBLT:

- [XAudio API](doc/xaudio.md)
- [Automatic Updates](doc/updates.md)
- [XML Tweaker](doc/tweaker.md)
- [XML Configuration Files](doc/xml.md)

